const mongoose = require("mongoose");


// Order Schema
const orderSchema = new mongoose.Schema({
	userId: 
	{ 
		type: String,
        required: [true, "User ID is required."]
	},
	firstName: 
	{
		type: String,
		required: [true, "First name is required."],
		trim: true,
		minlength: [2, "First name must have at least 2 characters"],
		maxlength: [50, "First name can have at most 50 characters"],
	},
	middleName: 
	{
		type: String,
		required: [true, "Middle name is required."],
		trim: true,
		minlength: [2, "Middle name must have at least 2 characters"],
		maxlength: [50, "Middle name can have at most 50 characters"],
	},
	lastName: 
	{
		type: String,
		required: [true, "Last name is required."],
		trim: true,
		minlength: [2, "Last name must have at least 2 characters"],
		maxlength: [50, "Last name can have at most 50 characters"],
	},
	products: 
	[
		{
			productId: 
			{ 
				type: String,
				required: [true, "Product ID is required."]
			},
			productName: 
			{
			  type: String,
			  required: [true, 'Product name is required.'],
			  trim: true,
			},
			productDescription: 
			{
			  type: String,
			  required: [true, 'Product description is required.'],
			  trim: true,
			},
			productCode: 
			{
			  type: String,
			  required: [true, 'Product Code is required.'],
			  trim: true,
			},
			productPrice: 
			{
			  type: Number,
			  required: [true, 'Product price is required.'],
			  min: [0, 'Product price cannot be negative.'],
			},
			quantity: 
			{ 
				type: Number,
				required: [true, "Quantity is required."],
			},
			subTotal: 
			{ 
				type: Number,
				required: [true, "Subtotal is required."],
			}
		}
	],
	totalAmount: 
	{ 
		type: Number,
        required: [true, "Total Amount is required."],
	},
	purchasedOn: 
	{ 
		type: Date,
		default: new Date(), 
	},
	updatedOn: 
	{
	  type: Date,
	  default: new Date(),
	},
});

// "module.exports" is a way for Node JS to treat the value as a package that can be used by other files
module.exports = mongoose.model("Order", orderSchema);