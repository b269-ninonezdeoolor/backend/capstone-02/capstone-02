const mongoose = require('mongoose');


// Product Schema
const productSchema = new mongoose.Schema({
  productName: 
  {
    type: String,
    required: [true, 'Product name is required.'],
    uppercase: true,
    trim: true,
  },
  productDescription: 
  {
    type: String,
    required: [true, 'Product description is required.'],
    trim: true,
  },
  productCode: 
  {
    type: String,
    required: [true, 'Product Code is required.'],
    trim: true,
  },
  productStocks: 
  {
    type: Number,
    required: [true, 'Product stocks is required.'],
  },
  productPrice: 
  {
    type: Number,
    required: [true, 'Product price is required.'],
    min: [0, 'Product price cannot be negative.'],
  },
  isActive: 
  {
    type: Boolean,
    default: true,
  },
  createdOn: 
  {
    type: Date,
    default: new Date(),
  },
  updatedOn: 
  {
    type: Date,
    default: new Date(),
  },
});

// "module.exports" is a way for Node JS to treat the value as a package that can be used by other files
module.exports = mongoose.model('Product', productSchema);
