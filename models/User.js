const mongoose = require("mongoose");


// User Schema
const userSchema = new mongoose.Schema({
	firstName: 
	{
		type: String,
		required: [true, "First name is required."],
		trim: true,
		minlength: [2, "First name must have at least 2 characters"],
		maxlength: [50, "First name can have at most 50 characters"],
	},
	middleName: 
	{
		type: String,
		required: [true, "Middle name is required."],
		trim: true,
		minlength: [2, "Middle name must have at least 2 characters"],
		maxlength: [50, "Middle name can have at most 50 characters"],
	},
	lastName: 
	{
		type: String,
		required: [true, "Last name is required."],
		trim: true,
		minlength: [2, "Last name must have at least 2 characters"],
		maxlength: [50, "Last name can have at most 50 characters"],
	},
	userName:
	{
		type: String,
		required: [true, "Username is required."],
		trim: true,
	},
	mobileNo: 
	{
		type: String,
		required: [true, "Mobile number is required."],
		trim: true,
	},
    email: 
	{
        type: String,
        required: [true, "Email address is required."],
        trim: true,
        match: [/^\S+@\S+\.\S+$/, "Invalid email address"]
    },
	password: 
	{
		type: String,
		required: [true, "Password is required."],
		minlength: [6, "Password must contain at least 6 characters"]
	},
	isAdmin: 
	{
		type: Boolean,
		default: false,
	},
});

module.exports = mongoose.model("User", userSchema);
