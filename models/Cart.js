const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "User ID is required."],
  },
  productId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Product",
    required: [true, "Product ID is required."],
  },
  productName: {
    type: String,
    trim: true,
  },
  productDescription: {
    type: String,
    trim: true,
  },
  productCode: {
    type: String,
    trim: true,
  },
  productPrice: {
    type: Number,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  subTotal: {
    type: Number,
    required: true,
    default: 0,
  }
});

module.exports = mongoose.model("Cart", cartSchema);