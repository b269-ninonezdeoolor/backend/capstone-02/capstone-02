// Dependencies
const express   = require("express");
const mongoose  = require("mongoose");
const cors      = require("cors");


// Server
const app = express();


// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// This allows us to use all the routes defined in the "userRoute.js" file
const userRoute     = require("./routes/userRoute");
// This allows us to use all the routes defined in the "productRoute.js" file
const productRoute  = require("./routes/productRoute");
// This allows us to use all the routes defined in the "orderRoute.js" file
const orderRoute   	= require("./routes/orderRoute");
// This allows us to use all the routes defined in the "cartRoute.js" file
const cartRoute   	= require("./routes/cartRoute");

// Allows all the user routes created in "userRoute.js" file to use "/users" as route (resources)
app.use("/users", userRoute);
// Allows all the user routes created in "courseRoute.js" file to use "/products" as route (resources)
app.use("/products", productRoute);
// Allows all the user routes created in "orderRoute.js" file to use "/orders" as route (resources)
app.use("/orders", orderRoute);
// Allows all the user routes created in "cartRoute.js" file to use "/orders" as route (resources)
app.use("/carts", cartRoute);


// Database
mongoose.connect("mongodb+srv://ninonezdeoolor11:admin123@zuitt-bootcamp.sul2u9q.mongodb.net/eCommerceAPI?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once("open", () => console.log(`Now connected to cloud database.`));


// Server listening
// Will use the defined port number for the application whenever environment variable is available to used port 4000 if none is defined.
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 5000, () => console.log(`The E-Commerce API is currently operational and listening on port ${process.env.PORT || 5000}...`));