const express           = require("express");
const router            = express.Router();
const auth              = require("../auth");
const orderController   = require("../controllers/orderController");


// User will create an order - Authenticated
router.post("/createOrder", auth.verify, (req, res) =>
{
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	const userId = 	auth.decode(req.headers.authorization).id;

	orderController.createOrder(req.body, isAdmin, userId).then(resultFromController => res.send(resultFromController));
});


// View all orders - Admin
router.get("/viewAllOrders", auth.verify, (req, res) =>
{
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	orderController.viewAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));
});


// View a specific order - Owner
router.get("/:orderId/viewAnOrder", auth.verify, (req, res) =>
{
	const data =
	{
		orderId: 	req.params.orderId,
		userId: 	auth.decode(req.headers.authorization).id
	}

	orderController.viewAnOrder(data).then(resultFromController => res.send(resultFromController));
});


// View user's order history - Owner
router.get("/:userId/viewUserCart", auth.verify, (req, res) =>
{
	const data =
	{
		userParam: 	req.params.userId,
		userAuth: 	auth.decode(req.headers.authorization).id
	}

	orderController.viewUserOrderHistory(data).then(resultFromController => res.send(resultFromController));
});


// Update quantity - Owner
router.put("/:orderId/updateQuantity", auth.verify, (req, res) =>
{
	const data =
	{
		orderId: 	req.params.orderId,
		orderInfo: 	req.body,
		userId: 	auth.decode(req.headers.authorization).id
	}

	orderController.updateQuantity(data).then(resultFromController => res.send(resultFromController));
});


// Remove a product - Owner
router.delete("/:orderId/removeProduct", auth.verify, (req, res) =>
{
	const data =
	{
		orderId: 	req.params.orderId,
		orderInfo: 	req.body,
		userId: 	auth.decode(req.headers.authorization).id
	}

	orderController.removeProduct(data).then(resultFromController => res.send(resultFromController));
});


// Add a product - Owner
router.post("/:orderId/addProduct", auth.verify, (req, res) =>
{
	const data =
	{
		orderId: 	req.params.orderId,
		orderInfo: 	req.body,
		userId: 	auth.decode(req.headers.authorization).id
	}

	orderController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;