const express           = require("express");
const router            = express.Router();
const auth              = require("../auth");
const cartController   = require("../controllers/cartController");


// Add to cart - User
router.post("/addToCart", auth.verify, (req, res) =>
{
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  const userId = auth.decode(req.headers.authorization).id;

  cartController.addToCart(req.body, userId, isAdmin)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(400).send(error.message));
});


// View cart - Owner
router.get("/viewCart", auth.verify, async (req, res) => 
{
  const userId = auth.decode(req.headers.authorization).id;
  const items = await cartController.viewCart(userId);
  res.send(items);
});


// Update Cart - Owner
router.put("/updateCart/:id", auth.verify, async (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  const cartItemId = req.params.id;
  const newQuantity = parseInt(req.body.quantity);

  if (isNaN(newQuantity) || newQuantity < 1) {
    res.status(400).send("Invalid quantity");
    return;
  }

  try {
    const updatedCartItem = await cartController.updateCartItem(userId, cartItemId, newQuantity);
    res.send(updatedCartItem);
  } catch (err) {
    res.status(400).send(err.message);
  }
});


// Remove Cart Item - Owner
router.delete("/removeCartItem/:id", auth.verify, async (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  const cartItemId = req.params.id;

  try {
    const updatedCartItem = await cartController.removeCartItem(userId, cartItemId);
    res.send(updatedCartItem);
  } catch (err) {
    res.status(400).send(err.message);
  }
});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;