const express               = require("express");
const router                = express.Router();
const auth                  = require("../auth");
const productController     = require("../controllers/productController");


// Create a product - Admin
router.post("/addProduct", auth.verify, (req, res) =>
{
	const data =
	{
		product: req.body,
		productCode: req.body.productCode,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// View all products - Admin only
router.get("/viewAllProducts", auth.verify, (req, res) =>
{
	const data =
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.viewAllProducts(data).then(resultFromController => res.send(resultFromController));
});


// View archived products - Admin only
router.get('/archivedProducts', auth.verify, async (req, res) => {
  try {
    const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    const archivedProducts = await productController.viewArchivedProducts(data);
    res.send(archivedProducts);
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
});

// View unarchived products - Admin only
router.get('/unarchivedProducts', auth.verify, async (req, res) => {
  try {
    const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    const unarchivedProducts = await productController.viewUnarchivedProducts(data);
    res.send(unarchivedProducts);
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
});


// Count all products - Admin only
router.get("/count", auth.verify, (req, res) =>
{
    const data =
    {
      isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productController.countAllProducts(data).then(resultFromController => res.send(resultFromController));
});


// View all active products - User Only
router.get("/viewAllActiveProducts", auth.verify, (req, res) =>
{
	const data =
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.viewAllActiveProducts(data).then(resultFromController => res.send(resultFromController));
});


// View all inactive products - Authenticated
router.get("/viewAllInactiveProducts", auth.verify, (req, res) =>
{
	const data =
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.viewAllInactiveProducts(data).then(resultFromController => res.send(resultFromController));
});


// View a specific product - Authenticated
router.get("/:productId/viewAproduct", auth.verify, (req, res) =>
{
	const data =
	{
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.viewAproduct(data).then(resultFromController => res.send(resultFromController));
});


// Update a product - Admin
router.put("/:productId/updateProduct", auth.verify, (req, res) =>
{
	const data =
	{
		productId: req.params.productId,
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController));
});


// Archive a product - Admin
router.patch("/:productId/archive", auth.verify, (req, res) =>
{
	const data =
	{
		productId: req.params.productId,
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});


// Unarchive a product - Admin
router.patch("/:productId/unArchive", auth.verify, (req, res) =>
{
	const data =
	{
		productId: req.params.productId,
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.unArchiveProduct(data).then(resultFromController => res.send(resultFromController));
});


// Delete a product - Admin
router.delete("/:productId/deleteProduct", auth.verify, (req, res) =>
{
	const data =
	{
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.deleteProduct(data).then(resultFromController => res.send(resultFromController));
});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;