const express           = require("express");
const router            = express.Router();
const auth              = require("../auth");
const userController    = require("../controllers/userController");


// Check if email already exists
router.post("/checkEmail", (req, res) =>
{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


// User Registration
router.post("/register", (req, res) =>
{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// User Authentication
router.post("/login", (req, res) =>
{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// 	User details
router.get("/details", auth.verify, (req, res) => 
{
	const userData = auth.decode(req.headers.authorization);

userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});


// View all users - Admin
router.get("/viewAllUsers", auth.verify, (req, res) =>
{
	const data =
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.viewAllUsers(data).then(resultFromController => res.send(resultFromController));
});


// View a specific user - Admin
router.get("/:userId/viewAuser", auth.verify, (req, res) =>
{
	const data =
	{
		userId: req.params.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.viewAuser(data).then(resultFromController => res.send(resultFromController));
});


// Set user as admin - Admin
router.patch("/:userId/setToAdmin", auth.verify, (req, res) =>
{
	const data =
	{
		userId: req.params.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.setToAdmin(data).then(resultFromController => res.send(resultFromController));
});


// Set user as regular user - Admin
router.patch("/:userId/setToUser", auth.verify, (req, res) =>
{
    const data =
    {
        userId: req.params.userId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    userController.setToUser(data).then(resultFromController => res.send(resultFromController));
});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;