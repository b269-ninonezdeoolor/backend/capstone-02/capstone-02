const User      = require("../models/User");
const Product   = require("../models/Product");
const Order     = require("../models/Order");
const Cart     = require("../models/Cart");


// User will create an order - Authenticated
module.exports.createOrder = async (data, isAdmin, userId) => 
{
    // Get the User data from the db using the provided user ID
    let user = await User.findById(userId);

    // If user ID is not admin
    if(!isAdmin)
    {
        // Define the products array as an empty array
        let products = [];

        // Loop through each order item in the input data and add it to the products array
        for(let i = 0; i < data.products.length; i++)
        {
            let productInfo = data.products[i];

            // Find the product by ID
            const product = await Product.findById(productInfo.productId);

            // Sub total from the price * quantity of each product
            let subTotal = product.productPrice * productInfo.quantity;

            // Create a new product object and add it to the products array
            products.push({
                            productId:             productInfo.productId,
                            productName:           product.productName,
                            productDescription:    product.productDescription,
                            productCode:           product.productCode,
                            productStocks:         product.productStocks,
                            productPrice:          product.productPrice,
                            quantity:              productInfo.quantity,
                            subTotal:              subTotal
            });
        }

        // Declare a 0 value for the totalAmount variable
        let totalAmount = 0;

        // Loop through each product in the products array and add its subTotal to the totalAmount
        for(let i = 0; i < products.length; i++)
        {
            totalAmount += products[i].subTotal;
        }

        // Create a new order object and set its properties
        let order = new Order({
                                userId:         userId,
                                firstName:      user.firstName,
                                middleName:     user.middleName,
                                lastName:       user.lastName,
                                products:       products,
                                totalAmount:    totalAmount
        });

        // Save the order details in the database
        await order.save();
        let message = Promise.resolve(user.firstName + " " + user.middleName + " " + user.lastName + " " + "created an order.");
        return message.then((value) =>
        {
            return {value};
        });
    }
    // If user ID is admin
    let message = Promise.resolve("Admin is not allowed to perform this action!");
    return message.then((value) =>
    {
        return {value};
    });
};


// View all orders - Admin
module.exports.viewAllOrders = async (isAdmin) =>
{
    // User is an admin
    if(isAdmin)
    {
        const viewAllOrders = await Order.find({});
        
        if (viewAllOrders.length > 0)
        {
            return viewAllOrders;
        }
        else
        {
            return {value: "There are currently no orders created."};
        }
    }
	// User is not an admin
	let message = Promise.resolve("Access denied! You need admin privileges to perform this action.");
	return message.then((value) =>
	{
		return {value};
	});
}


// View a specific order - Owner
module.exports.viewAnOrder = async (data) =>
{
    // Get the Order data from the db using the provided order ID
    const order = await Order.findById(data.orderId);

    // If the user is the owner of the order
    if(!order)
    {
        return {value: "Invalid order ID or this order does not exist."};
    }
    else if(order && order.userId === data.userId)
    {        
        const result = await Order.findById(data.orderId);
        return result;
    }
    // If user is not the owner of the order
    else
    {
        return {value: "Access denied! You're not the owner of this order."}; 
    }
};


// View user's order history - Owner
module.exports.viewUserOrderHistory = async (data) =>
{
    // Get the Order data from the db using the provided user ID token
    const user = await User.findById(data.userAuth);

    // Get the Order data from the db using the provided user ID params
    const userInfo = await User.findById(data.userParam);

    // If the user is the owner of the order
    if(userInfo && user._id.toString() === data.userParam)
    {
        const orderHistory = await Order.find({userId: data.userParam});
        if (orderHistory.length > 0)
        {
            return orderHistory;
        }
        else
        {
            return {value: "There are currently no orders created for this user."};
        }
    }
    // If user is not the owner of the order
    else
    {
        return {value: "Access denied! You're not the owner of this order."}; 
    }
}


// Update quantity - Owner
module.exports.updateQuantity = async (data) => 
{
    // Get the User data from the db using the provided user ID
    let user = await User.findById(data.userId);

    // Find the user's order in the database using the user ID
    let userAuth = await Order.findOne({userId: data.userId});

    // If the user is the owner of the order
    if(userAuth && user._id.toString() === data.userId)
    {
        // Get the Order data from the db using the provided order ID
        const order = await Order.findById(data.orderId);

        // If the order is valid or does exists
        if(order)
        {
            // Define the updatedOrders array as an empty array
            const updatedOrders = [];

            // Initialize totalAmount to 0
            let totalAmount = 0;

            for (let i = 0; i < data.orderInfo.products.length; i++)
            {
                // To get the price from the Product collection
                let product = await Product.findById(data.orderInfo.products[i].productId);

                // To get the subTotal
                let subTotal = product.productPrice * data.orderInfo.products[i].quantity;

                // To get the totalAmount
                totalAmount += subTotal;

                const filter = 
                { 
                    _id: data.orderId, 
                    userId: data.userId, 
                    "products.productId": data.orderInfo.products[i].productId, 
                };
                const update = 
                {
                    $set: 
                    {
                        "products.$.productName":          product.productName,
                        "products.$.productDescription":   product.productDescription,
                        "products.$.productCode":          product.productCode,
                        "products.$.productStocks":        product.productStocks,
                        "products.$.productPrice":         product.productPrice,
                        "products.$.quantity":             data.orderInfo.products[i].quantity,
                        "products.$.subTotal":             subTotal,
                        "totalAmount":                     totalAmount,
                        "updatedOn":				       new Date()
                    },
                };
                const options = { new: true };
                const updatedOrder = await Order.findOneAndUpdate(filter, update, options);

                // Add each updated order to array
                updatedOrders.push(updatedOrder);
            }
            let message = Promise.resolve(user.firstName + " " + user.middleName + " " + user.lastName + " " + "updated the product quantity successfully.");
            return message.then((value) =>
            {
                return {value};
            });
        }
        // If the order ID is invalid or does not exist
        else
        {
            console.log("Order ID is invalid or does not exist.");
        }
    }
    // If user is not the owner of the order
    else
    {
        return {value: "Access denied! You're not the owner of this order."}; 
    }
};


// Remove a product - Owner
module.exports.removeProduct = async (data) => 
{
    // Get the User data from the db using the provided user ID
    let user = await User.findById(data.userId);

    // Find the user's order in the database using the user ID
    let userAuth = await Order.findOne({userId: data.userId});

    // If the user is the owner of the order
    if(userAuth && user._id.toString() === data.userId)
    {
        // Get the Order data from the db using the provided order ID
        const order = await Order.findById(data.orderId);

        // If the order is valid or does exists
        if(order)
        {
            const updatedOrders = [];

            for (let i = 0; i < data.orderInfo.products.length; i++)
            {
                const filter         = { _id: data.orderId, userId: data.userId, "products.productId": data.orderInfo.products[i].productId };
                const update         = { $pull: { "products": { "productId": data.orderInfo.products[i].productId, } } };
                const options        = { new: true };
                const updatedOrder   = await Order.findOneAndUpdate(filter, update, options);
                updatedOrders.push(updatedOrder);


                let totalAmount = 0;

                for (let j = 0; j < updatedOrder.products.length; j++) 
                {
                    const product   = await Product.findById(updatedOrder.products[j].productId);
                    const quantity  = updatedOrder.products[j].quantity;
                    const price     = product.productPrice;
                    const subTotal  = quantity * price;
                    totalAmount     += subTotal;
                }

                const newFilter       = { _id: data.orderId, userId: data.userId, };
                const newUpdate       = { $set: { "totalAmount": totalAmount } };
                const newOptions      = { new: true };
                const newUpdatedOrder = await Order.findOneAndUpdate(newFilter, newUpdate, newOptions);
                updatedOrders.push(newUpdatedOrder);
            }
            let message = Promise.resolve(user.firstName + " " + user.middleName + " " + user.lastName + " " + "removed the products from the cart successfully.");
            return message.then((value) => 
            {
                return { value };
            });
        }
        // If the order ID is invalid or does not exist
        else
        {
            return {value: "Order ID is invalid or does not exist."};
        }
    }
    // If user is not the owner of the order
    else
    {
        return {value: "Access denied! You're not the owner of this order."}; 
    }
};


// Add a product - Owner
module.exports.addProduct = async (data) => 
{
    // Get the User data from the db using the provided user ID
    let user = await User.findById(data.userId);

    // Find the user's order in the database using the user ID
    let userAuth = await Order.findOne({userId: data.userId});

    // If the user is the owner of the order
    if(userAuth && user._id.toString() === data.userId)
    {
        // Get the Order data from the db using the provided order ID
        const order = await Order.findById(data.orderId);

        // If the order is valid or does exists
        if(order)
        {
            const updatedOrders = [];

            let totalAmount = 0;

            for (let i = 0; i < data.orderInfo.products.length; i++) 
            {
                // To get the price from the Product collection
                const product = await Product.findById(data.orderInfo.products[i].productId);

                const price         = product.productPrice;
                const quantity      = data.orderInfo.products[i].quantity;
                const subTotal      = quantity * price;

                const filter          = { _id: data.orderId, userId: data.userId };
                const update          = 
                { 
                    $push: 
                        { 
                            products: 
                            { 
                                productId:              data.orderInfo.products[i].productId,
                                productName:            product.productName,
                                productDescription:     product.productDescription,
                                productCode:            product.productCode,
                                productStocks:          product.productStocks,
                                productPrice:           price,
                                quantity:               quantity,
                                subTotal:               subTotal
                            }
                        }
                };
                
                const options         = { new: true };
                const updatedOrder    = await Order.findOneAndUpdate(filter, update, options);
                updatedOrders.push(updatedOrder);

                let newTotalAmount = 0;
                
                for (let j = 0; j < updatedOrder.products.length; j++) 
                {
                    const product       = await Product.findById(updatedOrder.products[j].productId);
                    
                    const price         = product.productPrice;
                    const quantity      = updatedOrder.products[j].quantity;
                    const subTotal      = quantity * price;
                    newTotalAmount      += subTotal;
                }

                const newFilter       = { _id: data.orderId, userId: data.userId };
                const newUpdate       = { $set: { totalAmount: newTotalAmount } };
                const newOptions      = { new: true };
                const newUpdatedOrder = await Order.findOneAndUpdate(newFilter, newUpdate, newOptions);
                updatedOrders.push(newUpdatedOrder);
            }

            let message = Promise.resolve(user.firstName + " " + user.middleName + " " + user.lastName + " " + "added the products to the cart successfully.");
            return message.then((value) => 
            {
                return { value };
            });
        }
        // If the order ID is invalid or does not exist
        else
        {
            return {value: "Order ID is invalid or does not exist."};
        }
    }
    // If user is not the owner of the order
    else
    {
        return {value: "Access denied! You're not the owner of this order."}; 
    }
};
