const bcrypt    = require('bcrypt');
const auth      = require("../auth");
const User      = require("../models/User");


// Check if email already exists
module.exports.checkEmailExists = (reqBody) =>
{
	return User.find({email: reqBody.email}).then(result =>
	{
        // User exist
		if(result.length > 0)
		{
            return true;
		}
        // User does not exist
		else
		{
            return false;
		};
	});
};


// User Registration
module.exports.registerUser = (reqBody) => 
{	
	return User.findOne({ email: reqBody.email }).then((user) => 
	{
		// User exists
		if(user && user.email === reqBody.email) 
		{
			return true;
		}
		// User does not exist
		else
		{
			let newUser = new User({
			firstName: 		reqBody.firstName,
			middleName: 	reqBody.middleName,
			lastName: 		reqBody.lastName,
			userName: 		reqBody.userName,
			mobileNo: 		reqBody.mobileNo,
			email: 			reqBody.email,
			password: 		bcrypt.hashSync(reqBody.password, 10)
			});

			return newUser.save().then((user, error) => 
			{
				if(error) 
				{
					return false;
				}
				else
				{
					return true;
				};
			});
		}
	});
};
  


// User Authentication
module.exports.loginUser = (reqBody) =>
{
	return User.findOne({email: reqBody.email}).then(result =>
	{
		// User does not exist
		if(result == null)
		{
            let message = Promise.resolve("Incorrect email or password.");
            return message.then((value) =>
            {
                return {value};
            });
		}
		// User exist
		else
		{
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// If the password match/result of the above code is true
			if(isPasswordCorrect)
			{
				return {access: auth.createAccessToken(result)}
			}
			// If password do not match
			else
			{
				return false;
			};
		};
	});
};


// 	User details
module.exports.getProfile = (data) => 
{
	return User.findById(data.userId).select("-password").then(result => 
	{
		return result;
	});
};


// View all users - Admin
module.exports.viewAllUsers = (data) =>
{
	// User is admin
	if(data.isAdmin)
	{
		return User.find({}).select("-password").then(result =>
		{
			return result;
		});
	}
	// User is not an admin
	let message = Promise.resolve("Access denied! You need admin privileges to perform this action.");
	return message.then((value) =>
	{
		return {value};
	});
};


// View a specific user - Admin
module.exports.viewAuser = async (data) =>
{
	// User is admin
	if(data.isAdmin)
    {
		// Get the User data from the db using the provided user ID
		let user = await User.findById(data.userId);

		// If user is all true
		if(user._id.toString() === data.userId)
		{
			const result = await User.findById(data.userId).select("-password");
			return result;
		}
		// User is invalid or does not exist
		else
		{
			let message = Promise.resolve("User is invalid or does not exist.");
			return message.then((value) =>
			{
				return {value};
			});
		}
	}
    // User is not an admin
    return { value: "Access denied! You need admin privileges to perform this action." };
};


// Set user as admin - Admin
module.exports.setToAdmin = async (data) =>
{
	// User is admin
    if(data.isAdmin)
    {
		// Get the User data from the db using the provided user ID
		let user = await User.findById(data.userId);

		// If user is all true
		if(user && user._id.toString() === data.userId && user.isAdmin)
		{
			return { value: "This user is already an admin."};
		}
		else if(user && user._id.toString() === data.userId)
		{

			const setToAdmin = 
			{
				isAdmin: true
			};

			// findByIdAndUpdate(document ID, updatesToBeApplied)
			let updatedUser = await User.findByIdAndUpdate(data.userId, setToAdmin);
			if(updatedUser)
			{
				return { value: "This user is now an admin." };
			}
		}
		// User is invalid or does not exist
		else
		{
			return { value: "User is invalid or does not exist."};
		}
    }
    // User is not an admin
    return { value: "Access denied! You need admin privileges to perform this action." };
};


// Set user as regular user - Admin
module.exports.setToUser = async (data) =>
{
    // User is admin
    if(data.isAdmin)
    {
        // Get the User data from the db using the provided user ID
        let user = await User.findById(data.userId);

        // If user is all true
        if(user && user._id.toString() === data.userId && !user.isAdmin)
        {
            return { value: "This user is already a regular user."};
        }
        else if(user && user._id.toString() === data.userId)
        {

            const setToUser = 
            {
                isAdmin: false
            };

            // findByIdAndUpdate(document ID, updatesToBeApplied)
            let updatedUser = await User.findByIdAndUpdate(data.userId, setToUser);
            if(updatedUser)
            {
                return { value: "This user is now a user." };
            }
        }
        // User is invalid or does not exist
        else
        {
            return { value: "User is invalid or does not exist."};
        }
    }
    // User is not an admin
    return { value: "Access denied! You need admin privileges to perform this action." };
};