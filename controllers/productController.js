const Product   = require("../models/Product");


// Create a product - Admin
module.exports.addProduct = (data) => {
  // User is an admin
  if (data.isAdmin) {
    // Check if the productCode is already exists in the database
    return Product.findOne({ productCode: data.productCode }).then((product) => {
      // Product exists
      if (product && product.productCode === data.productCode) {
        return { message: "Product code already exists in the database." };
      }
      // Product does not exist
      else {
        // Creates a variable "newProduct" and instantiates a new "Product" object using the mongoose model
        // Uses the information from the request body to provide all the necessary information
        let newProduct = new Product({
          productName: data.product.productName,
          productDescription: data.product.productDescription,
          productCode: data.product.productCode,
          productStocks: data.product.productStocks,
          productPrice: data.product.productPrice,
        });
        // Saves the created object to our database
        return newProduct.save().then((product, error) => {
          // Product creation failed
          if (error) {
            return {error: "Product creation failed."};
          }
          // Product creation successful
          else {
            return { message: "Product was created successfully." };
          }
        });
      }
    });
  }
  // User is not an admin
  let message = Promise.resolve("Access denied! You need admin privileges to perform this action.");
  return message.then((value) => {
    return { value };
  });
};


// View all products - Admin only
module.exports.viewAllProducts = async (data) =>
{
	// If user is Admin
	if(data.isAdmin)
	{
		const viewallProducts = await Product.find({})
			.sort({ createdOn: -1 }); // sort products in descending order based on createdOn field
 		if (viewallProducts.length > 0)
		{
			return viewallProducts;
		}
		else
		{
			return {value: "There are currently no products to display."};
		}
	}
	// If user is not Admin
	let message = Promise.resolve("Access denied! You need admin privileges to perform this action.");
	return message.then((value) =>
	{
		return {value};
	});
};


// View archived products - Admin only
module.exports.viewArchivedProducts = async (data) => {
  // If user is Admin
  if (data.isAdmin) {
    const archivedProducts = await Product.find({ isActive: false })
      .sort({ createdOn: -1 });
     if (archivedProducts.length > 0) {
      return archivedProducts;
    } else {
      return { value: "There are currently no archived products to display." };
    }
  }
   // If user is not Admin
  let message = "Access denied! You need admin privileges to perform this action.";
  return { value: message };
};
 // View unarchived products - Admin only
module.exports.viewUnarchivedProducts = async (data) => {
  // If user is Admin
  if (data.isAdmin) {
    const unarchivedProducts = await Product.find({ isActive: true })
      .sort({ createdOn: -1 });
     if (unarchivedProducts.length > 0) {
      return unarchivedProducts;
    } else {
      return { value: "There are currently no unarchived products to display." };
    }
  }
   // If user is not Admin
  let message = "Access denied! You need admin privileges to perform this action.";
  return { value: message };
};


// Count all products - Admin only
module.exports.countAllProducts = async (data) =>
{
	// If user is Admin
	if(data.isAdmin)
	{
		const totalProducts = await Product.countDocuments({});
		return {total: totalProducts};
	}
	// If user is not Admin
	let message = Promise.resolve("Access denied! You need admin privileges to perform this action.");
	return message.then((value) =>
	{
		return {value};
	});
};


// View all active products - User Only
module.exports.viewAllActiveProducts = async (data) =>
{
	// To check if it's an authenticated user
	if(!data.isAdmin)
	{
		const activeProducts = await Product.find({isActive: true});
		
		if (activeProducts.length > 0)
		{
			return activeProducts;
		}
		else
		{
			return {value: "There are currently no active products to display."};
		}
	}
	// User is not registered yet
	let message = Promise.resolve("Admin is not allowed to perform this action!");
	return message.then((value) =>
	{
		return {value};
	});
};


// View all inactive products - Authenticated
module.exports.viewAllInactiveProducts = async (data) =>
{
	// To check if it's an authenticated user
	if(data.isAdmin || !data.isAdmin)
	{
		const inActiveProducts = await Product.find({isActive: false});
		
		if (inActiveProducts.length > 0)
		{
			return inActiveProducts;
		}
		else
		{
			return {value: "There are currently no inactive products to display."};
		}
	}
	// User is not registered yet
	let message = Promise.resolve("Access denied! You need to register to perform this action.");
	return message.then((value) =>
	{
		return {value};
	});
};


// View a specific product - Authenticated
module.exports.viewAproduct = (data) => {
  // To check if it's an authenticated user
  if (data.isAdmin || !data.isAdmin) {
    // Check the productId in the Product collection exists
    return Product.findById(data.productId).then((result) => {
      // Check if product ID exists
      if (result) {
        return { product: result }; // Return object with "product" property
      } else {
        return { product: null }; // Return object with "product" property
      }
    });
  }
  // User is not registered yet
  let message = Promise.resolve(
    "Access denied! You need to register to perform this action."
  );
  return message.then((value) => {
    return { product: null }; // Return object with "product" property
  });
};


// Update a product - Admin
module.exports.updateProduct = (data) => {
  if (data.isAdmin) {
    return Product.findById(data.productId)
      .then((result) => {
        if (result) {
          let isActive = data.product.productStock === 0 ? false : true;
          let updateProduct = {
            productName: data.product.productName,
            productDescription: data.product.productDescription,
            productStocks: data.product.productStocks,
            productPrice: data.product.productPrice,
            isActive: isActive,
            updatedOn: new Date(),
          };
          // Check if product ID exist and update if it does
          return Product.findByIdAndUpdate(data.productId, updateProduct)
            .then((product, error) => {
              if (error) {
                return { success: false, message: "Failed to update product" };
              } else {
                // Return the updated product object and success message
                const updatedProduct = { message: "Product was updated successfully.", product };
                return { success: true, ...updatedProduct };
              }
            });
        } else {
          return { success: false, message: "Product is invalid or does not exist." };
        }
      })
      .catch((error) => ({ success: false, message: "Failed to update product" }));
  } else {
    return { success: false, message: "Access denied! You need admin privileges to perform this action." };
  }
};



// Archive a product - Admin
module.exports.archiveProduct = (data) =>
{
	if(data.isAdmin)
	{
		// Check the productId in the Product collection exists
		return Product.findById(data.productId).then(result =>
		{
			// Check if product ID is already in isActive: false status
			if(result.isActive === false)
			{
				let message = Promise.resolve("Product status is already not active.");
				return message.then((value) =>
				{
					return {value};
				});
			}
			// Check if product ID is all true
			else if(result && result.isActive === true)
			{
				let archiveProduct = 
				{
					isActive: false
				};
				// findByIdAndUpdate(document ID, updatesToBeApplied)
				return Product.findByIdAndUpdate(data.productId, archiveProduct).then((product, error) =>
				{
					if(error)
					{
						return false;
					}
					else
					{
						let message = Promise.resolve("Product was successfully archived!");
						return message.then((value) =>
						{
							return {value};
						});
					};
				});
			}
			// Product ID does not exist
			else
			{
				return {value: "Product is invalid or does not exist."};
			}
		});
	}
	// User is not an admin
	let message = Promise.resolve("Access denied! You need admin privileges to perform this action.");
	return message.then((value) =>
	{
		return {value};
	});
};


// Unarchive a product - Admin
module.exports.unArchiveProduct = (data) =>
{
	if(data.isAdmin)
	{
		// Check the productId in the Product collection exists
		return Product.findById(data.productId).then(result =>
		{
			// Check if product ID is already in isActive: true status
			if(result.isActive === true)
			{
				let message = Promise.resolve("Product status is already active.");
				return message.then((value) =>
				{
					return {value};
				});
			}
			// Check if product ID is all true
			else if(result && result.isActive === false)
			{
				let unArchiveProduct = 
				{
					isActive: true
				};
				// findByIdAndUpdate(document ID, updatesToBeApplied)
				return Product.findByIdAndUpdate(data.productId, unArchiveProduct).then((product, error) =>
				{
					if(error)
					{
						return false;
					}
					else
					{
						let message = Promise.resolve("Product was successfully unarchived!");
						return message.then((value) =>
						{
							return {value};
						});
					};
				});
			}
			// Product ID does not exist
			else
			{
				return {value: "Product is invalid or does not exist."};
			}
		});
	}
	// User is not an admin
	let message = Promise.resolve("Access denied! You need admin privileges to perform this action.");
	return message.then((value) =>
	{
		return {value};
	});
};


// Delete a product - Admin
module.exports.deleteProduct = (data) =>
{
	// User is an admin
	if(data.isAdmin)
	{
		return Product.findByIdAndRemove(data.productId).then(result =>
		{
			if(result)
			{
				return {value: "Product was successfully deleted!"};
			}
			else
			{
				return {value: "Product is invalid or does not exist!"};
			}
		});
	}
	// User is not an admin
	let message = Promise.resolve("Access denied! You need admin privileges to perform this action.");
	return message.then((value) =>
	{
		return {value};
	});
};