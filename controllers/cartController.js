const User      = require("../models/User");
const Product   = require("../models/Product");
const Order     = require("../models/Order");
const Cart     = require("../models/Cart");


// Add to cart - User
module.exports.addToCart = async (data, userId, isAdmin) => {
  try {
    // Check if user is an admin
    if (isAdmin) {
      throw new Error('Admin cannot add items to the cart.');
    }

    const user = await User.findById(userId);
    const { productId, quantity } = data;

    // Find the product by ID
    const product = await Product.findById(productId);

    if (!product) {
      throw new Error('Invalid product ID');
    }

    let cartItem = await Cart.findOne({ userId: user._id, productId: productId });

    // Check if the product is already present in the cart
    if (cartItem) {
      // Increment the quantity of the existing cart item
      cartItem.quantity += quantity;
      const subTotal = cartItem.quantity * product.productPrice;
      cartItem.subTotal = subTotal;
      cartItem.totalAmount = cartItem.subTotal;

      await cartItem.save();

      // Fetch all cart items for the user
      const cartItems = await Cart.find({ userId: user._id });

      return {
        message: `${user.firstName} ${user.middleName} ${user.lastName} added ${quantity} ${product.productName} to the cart.`,
        cartId: cartItem._id,
        cartItems: cartItems
      };
    }
    else 
    {
      const subTotal = quantity * product.productPrice;
      const totalAmount = subTotal;

      // Create a new cart object and set its properties
      let cart = new Cart({
        userId: user._id,
        firstName: user.firstName,
        middleName: user.middleName,
        lastName: user.lastName,
        productId: productId,
        productName: product.productName,
        productDescription: product.productDescription,
        productCode: product.productCode,
        productPrice: product.productPrice,
        quantity: quantity,
        subTotal: subTotal,
        totalAmount: totalAmount
      });

      // Save the cart details in the database
      await cart.save();

      // Fetch all cart items for the user
      const cartItems = await Cart.find({ userId: user._id });

      return {
        message: `${user.firstName} ${user.middleName} ${user.lastName} added ${quantity} ${product.productName} to the cart.`,
        cartId: cart._id,
        cartItems: cartItems
      };
    }
  } catch (err) {
    throw new Error(`Failed to add to cart: ${err.message}`);
  }
};


// View cart - Owner
module.exports.viewCart = async (userId) => {
  const user = await User.findById(userId);

  // If the user is the owner of the order
  if (user && user._id.toString() === userId) {
    const items = await Cart.find({ userId });
    return { items };
  } else {
    return { value: "Access denied! You're not the owner of this order." };
  }
};


// Update Cart - Owner
module.exports.updateCartItem = async (userId, cartItemId, newQuantity) => {
  try {
    const user = await User.findById(userId);
    const cartItem = await Cart.findOne({ userId: user._id, _id: cartItemId });

    if (!cartItem) {
      throw new Error('Cart item not found.');
    }
    const product = await Product.findById(cartItem.productId);
    if (!product) {
      throw new Error('Invalid product ID');
    }

    const oldQuantity = cartItem.quantity;
    const newSubTotal = newQuantity * product.productPrice;
    const newTotalAmount = (cartItem.totalAmount - cartItem.subTotal) + newSubTotal;

    if (newQuantity < 1) // handle invalid quantity
    {
      throw new Error('Invalid quantity');
    }
    else
    {
      cartItem.quantity = newQuantity;
      cartItem.subTotal = newSubTotal;
      cartItem.totalAmount = newTotalAmount;
      await cartItem.save();
    }

    const cartItems = await Cart.find({ userId: user._id });
    const totalAmount = cartItems.reduce((total, item) => total + item.subTotal, 0);

    const updatedCartItem = {
      message: `${user.firstName} ${user.middleName} ${user.lastName}'s cart updated.`,
      cartItems: cartItems,
      totalAmount: totalAmount
    };

    console.log("Response:", updatedCartItem); // log the response to the console
    return JSON.stringify(updatedCartItem);
  } catch (err) {
    console.log("Error:", err.message); // log the error to the console
    throw new Error(err.message); // throw the error message as a string
  }
};


// Remove Cart Item - Owner
module.exports.removeCartItem = async (userId, cartItemId) => {
  console.log("User ID:", userId); // log the user ID to the console
  console.log("Cart Item ID:", cartItemId); // log the cart item ID to the console
  try {
    const user = await User.findById(userId);
    console.log("User:", user); // log the user to the console
    const cartItem = await Cart.findOne({ userId: user._id, _id: cartItemId });
    console.log("Cart Item:", cartItem); // log the cart item to the console

    if (!cartItem) {
      throw new Error('Cart item not found.');
    }

    await Cart.deleteOne({ userId: user._id, _id: cartItemId });
    
    const cartItems = await Cart.find({ userId: user._id });
    const totalAmount = cartItems.reduce((total, item) => total + item.subTotal, 0);

    const updatedCartItem = {
      message: `${user.firstName} ${user.middleName} ${user.lastName}'s cart updated.`,
      cartItems: cartItems,
      totalAmount: totalAmount
    };

    console.log("Response:", updatedCartItem); // log the response to the console
    return updatedCartItem;
  } catch (err) {
    console.log("Error:", err.message); // log the error to the console
    throw new Error(err.message); // throw the error message as a string
  }
};